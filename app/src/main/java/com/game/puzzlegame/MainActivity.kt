package com.game.puzzlegame

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.MotionEvent
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.game.puzzlegame.databinding.ActivityMainBinding
import com.game.puzzlegame.utility.dialog.MainDialog
import com.game.puzzlegame.utility.extension.toNumberComma

class MainActivity : AppCompatActivity() {
    private val binding: ActivityMainBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        GameObject.emptyText = getString(R.string.btn_title_empty)

        GameObject.setView(binding)
        initListener()
    }

    private fun initListener(){
        GameObject.setOnTouchBtn()
        GameObject.setOnWinListener {
            MainDialog.newInstance(
                getString(R.string.dialog_congratulation_title).format(GameObject.count.toString().toNumberComma()),
                2000)
                .show(supportFragmentManager, "win")
        }
        binding.randomButton.setOnClickListener {
            GameObject.randomGame(this)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putStringArrayList("button_text", GameObject.getStateButton())
        outState.putInt("count", GameObject.count)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        GameObject.setStateButton(savedInstanceState.getStringArrayList("button_text")?: arrayListOf())
        GameObject.count = savedInstanceState.getInt("count")
        GameObject.setCount()
    }


}