package com.game.puzzlegame.utility.dialog

import android.app.Dialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import androidx.appcompat.app.AppCompatDialogFragment
import com.game.puzzlegame.databinding.DialogMainBinding

//
//  MainDialog.kt
//
//  puzzleGame
//
//  Created by Jong on 25/3/2023 AD.
//
class MainDialog: AppCompatDialogFragment() {
    private lateinit var binding: DialogMainBinding
    private var autoDismiss = -1L
    private var title = ""
    private var eventAutoDismissListener: (() -> Unit)? = null
    fun setOnAutoDismissListener(listener: () -> Unit) {
        eventAutoDismissListener = listener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window?.setGravity(Gravity.CENTER)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        isCancelable = true

        return dialog
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DialogMainBinding.inflate(inflater,container,false)
        return binding.root

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            restoreArguments(requireArguments())
        } else {
            restoreInstanceState(savedInstanceState)
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInstance()

    }

    private fun initInstance() {
        binding.dialogTitle.text = title
        if (autoDismiss != -1L){
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed(Runnable {
                if (dialog != null){
                    dismissAllowingStateLoss()
                }
                eventAutoDismissListener?.invoke()
            }, autoDismiss)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(KEY_TITLE, title)
        outState.putLong(KEY_AUTO_DISMISS, autoDismiss)
    }

    private fun restoreInstanceState(bundle: Bundle) {
        title = bundle.getString(KEY_TITLE, "")
        autoDismiss = bundle.getLong(KEY_AUTO_DISMISS, -1L)

    }

    private fun restoreArguments(bundle: Bundle) {
        title = bundle.getString(KEY_TITLE, "")
        autoDismiss = bundle.getLong(KEY_AUTO_DISMISS, -1L)

    }

    override fun onResume() {
        super.onResume()
        val width = (resources.displayMetrics.widthPixels * 0.9).toInt()
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.setLayout(width, height)
    }

    companion object {
        private const val KEY_TITLE = "key_title"
        private const val KEY_AUTO_DISMISS = "key_auto_dismiss"

        fun newInstance(title: String, autoDismiss: Long = -1): MainDialog {
            return MainDialog().apply {
                arguments = Bundle().apply {
                    putString(KEY_TITLE, title)
                    putLong(KEY_AUTO_DISMISS, autoDismiss)
                }
            }
        }

    }
}