package com.game.puzzlegame.utility.extension

import android.view.View

//
//  ViewExtension.kt
//
//  puzzleGame
//
//  Created by Jong on 25/3/2023 AD.
//
fun View.gone() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}