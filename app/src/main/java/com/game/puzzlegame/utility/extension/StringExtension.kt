package com.game.puzzlegame.utility.extension

import java.text.DecimalFormat

//
//  StringExtension.kt
//
//  puzzleGame
//
//  Created by Jong on 26/3/2023 AD.
//
fun String?.toNumberComma(): String {
    if (this.isNullOrEmpty()|| this == "null"){
        return "0"
    }
    val str = this.replace(",", "")
    val formatter = DecimalFormat("###,###,###,##0")
    formatter.negativePrefix = "- "
    val yourFormattedString = formatter.format(str.toFloat())
    return yourFormattedString
}