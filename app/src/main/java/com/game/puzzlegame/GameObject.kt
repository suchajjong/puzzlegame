package com.game.puzzlegame

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Button
import com.game.puzzlegame.databinding.ActivityMainBinding
import com.game.puzzlegame.utility.extension.gone
import com.game.puzzlegame.utility.extension.invisible
import com.game.puzzlegame.utility.extension.toNumberComma
import com.game.puzzlegame.utility.extension.visible

@SuppressLint("ClickableViewAccessibility")
object GameObject {
    private lateinit var binding: ActivityMainBinding
    var empty = 16
    var emptyText = ""
    var count = 0

    fun getStateButton():ArrayList<String>{
        val list = arrayListOf<String>()
        for (i in listViewButton){
            list.add(i.text.toString())
        }
        return list
    }
    fun setStateButton(list: ArrayList<String>){
        listViewButton.forEachIndexed { index, button ->
            button.text = list[index]
            if (list[index] == emptyText){
                empty = index+1
            }
            if (list[index] == emptyText){
                setEmptyBtn(button)
            }
            else{
                setNormalBtn(button)
            }
        }
    }

    private var eventWinListener: (() -> Unit)? = null
    fun setOnWinListener(listener: () -> Unit) {
        eventWinListener = listener
    }

    private var listViewButton = arrayListOf<Button>()

    fun setView(viewBind: ActivityMainBinding){
        binding = viewBind
        listViewButton = arrayListOf(
            binding.btn1,binding.btn2,binding.btn3,binding.btn4,
            binding.btn5,binding.btn6,binding.btn7,binding.btn8,
            binding.btn9,binding.btn10,binding.btn11,binding.btn12,
            binding.btn13,binding.btn14,binding.btn15,binding.btn16
        )
        emptyText = binding.root.context.getString(R.string.btn_title_empty)
        setCount()
    }
    fun clickBtn(position: Int){
        //บน
        if(empty == position - 4){
            listViewButton[position-5].text = listViewButton[position-1].text
            empty = position
            setEmptyBtn(listViewButton[position-1])
            setNormalBtn(listViewButton[position-5])
        }
        //ขวา
        else if(empty == position+1 && position%4 != 0){
            listViewButton[position].text = listViewButton[position-1].text
            empty = position
            setEmptyBtn(listViewButton[position-1])
            setNormalBtn(listViewButton[position])
        }
        //ล่าง
        else if(empty == position + 4){
            listViewButton[position+3].text = listViewButton[position-1].text
            empty = position
            setEmptyBtn(listViewButton[position-1])
            setNormalBtn(listViewButton[position+3])
        }
        //ซ้าย
        else if(empty == position - 1 && position%4 != 1){
            listViewButton[position-2].text = listViewButton[position-1].text
            empty = position
            setEmptyBtn(listViewButton[position-1])
            setNormalBtn(listViewButton[position-2])
        }
    }

    fun setOnTouchBtn(){
        listViewButton.forEachIndexed { index, button ->
            button.setOnTouchListener(object : OnSwipeTouchListener(binding.root.context){
                override fun onClick() {
                    super.onClick()
                    clickBtn(index+1)
                }
                override fun onSwipeLeft() {
                    super.onSwipeLeft()
                    swipeLeft(index+1)
                }
                override fun onSwipeRight() {
                    super.onSwipeRight()
                    swipeRight(index+1)
                }
                override fun onSwipeTop() {
                    super.onSwipeTop()
                    swipeTop(index+1)
                }
                override fun onSwipeBottom() {
                    super.onSwipeBottom()
                    swipeBottom(index+1)
                }

            })
        }
    }

    private fun checkWinGame(){
        if (isWin(binding.root.context)){
            eventWinListener?.invoke()
            count = 0
            setCount()
        }
    }

   private fun isWin(context: Context):Boolean{
       val listCheck = (1..15).toList().map { it.toString() }.toMutableList()
       listCheck.add(emptyText)
       listViewButton.forEachIndexed { index, button ->
           if(button.text != listCheck[index]){
               return false
           }
       }
       return true
    }


    private fun swipeRight(position: Int){
        //ขวา
        if(empty == position+1 && position%4 != 0){
            listViewButton[position].text = listViewButton[position-1].text
            empty = position
            setEmptyBtn(listViewButton[position-1])
            setNormalBtn(listViewButton[position])
        }

    }

    private fun swipeTop(position: Int){
        //บน
        if(empty == position - 4 && position > 4){
            listViewButton[position-5].text = listViewButton[position-1].text
            empty = position
            setEmptyBtn(listViewButton[position-1])
            setNormalBtn(listViewButton[position-5])
        }
    }
    private fun swipeBottom(position: Int){
        //ล่าง
        if(empty == position + 4 && position <= 12){
            listViewButton[position+3].text = listViewButton[position-1].text
            empty = position
            setEmptyBtn(listViewButton[position-1])
            setNormalBtn(listViewButton[position+3])
        }
    }
    private fun swipeLeft(position: Int){
        //ซ้าย
        if(empty == position - 1 && position%4 != 1){
            listViewButton[position-2].text = listViewButton[position-1].text
            empty = position
            setEmptyBtn(listViewButton[position-1])
            setNormalBtn(listViewButton[position-2])
        }
    }

    fun randomGame(context: Context){
        val listGame = (0..15).toList().toMutableList()
//        listGame.add(emptyText)
        listGame.shuffle()
        while (!isSolvable(listGame, (0..15).toList().toMutableList(), listGame.size)){
            listGame.shuffle()
        }
        val listGameString = listGame.map {
            if (it == 0){
                emptyText
            }
            else{
                it.toString()
            }
        }
        val list = arrayListOf<String>()
        list.addAll(listGameString)
        setStateButton(list)
        count = 0
        setCount()
    }

    fun inversions(list: List<Int>): Int {
        var inversions = 0
        val size = list.size
        // for every number we need to count:
        // - numbers less than chosen
        // - follow chosen number (by rows)
        for (i in 0 until size) {
            val n = list[i]
            if (n <= 1) {
                continue
            }
            for (j in i + 1 until size) {
                val m = list[j]
                if (m > 0 && n > m) {
                    inversions++
                }
            }
        }
        return inversions
    }
    private fun isSolvable(state: List<Int>, goal: List<Int>, width: Int): Boolean {
        val stateInversions = inversions(state)
        val goalInversions = inversions(goal)
        if (width % 2 == 0) {
            val goalZeroRowIndex = goal.indexOf(0) / width
            val startZeroRowIndex = state.indexOf(0) / width
            // if 'goalInversions' is even
            // 'stateInversions' and difference between 'goal' zero row index and zero row index in the 'state'
            // should have the same parity
            // if 'goalInversions' is odd, 'stateInversions' and difference must have different parity

            // since we're interested only in parity, an optimization is possible
            return goalInversions % 2 == (stateInversions + goalZeroRowIndex + startZeroRowIndex) % 2
        }
        // 'startInversions' should have the same parity as 'goalInversions'
        return stateInversions % 2 == goalInversions % 2
    }

    private fun setNormalBtn(btn: Button){
        btn.visible()
    }
    private fun setEmptyBtn(btn: Button){
        btn.text = emptyText
        btn.invisible()
        count += 1
        setCount()
        checkWinGame()
    }

    fun setCount(){
        binding.count.text = count.toString().toNumberComma()
    }


}