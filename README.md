ทดลองสร้าง 15 Puzzle Game สำหรับ android application

สามารถลองโหลด apk มาทดสอบได้ที่ [Link](https://gitlab.com/suchajjong/puzzlegame/-/blob/master/apk/puzzleGame.apk) นี้

ถ้า build ไม่ได้ ให้ลองปรับตัว Gradle JDK เป็น 11 (File -> Project Structure -> SDK Location -> GradleSettings)
